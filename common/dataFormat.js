let successFormat = {
	code: 200,
	message: '',
	success: true,
	data: []
}

let errorFormat = {
	code: 200,
	message: 'System Error',
	success: false,
	data: []
}

module.exports = { successFormat, errorFormat }