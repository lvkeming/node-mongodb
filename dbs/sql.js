// 新增、修改数据
const save = model => {
	return new Promise((resolve, reject) => {
		model.save((err, data) => {
			if (err) {
				reject(err)
			}
			resolve(data)
		})
	})
}

// 查询符合条件的全部数据
const query = (model, query = {}) => {
	return new Promise((resolve, reject) => {
		model.find(query, (err, data) => {
			if (err) {
				reject(err)
			}
			resolve(data)
		})
	})
}

// 查询符合条件的第一个数据
const hasOne = (model, query = {}) => {
	return new Promise((resolve, reject) => {
		model.findOne(query, (err, data) => {
			if (err) {
				reject(err)
			}
			resolve(data)
		})
	})
}

// 删除符合条件的第一个数据
const deleteOne = (model, query = {}) => {
	return new Promise((resolve, reject) => {
		model.deleteOne(query, (err, data) => {
			if (err) {
				reject(err)
			}
			resolve(data)
		})
	})
}

// 删除符合条件的全部数据
const deleteMany = (model, query) => {
	return new Promise((resolve, reject) => {
		model.deleteMany(query, (err, data) => {
			if (err) {
				reject(err)
			}
			resolve(data)
		})
	})
}

// 更新符合条件的第一个数据
const updateOne = (model, query, updateData) => {
	return new Promise((resolve, reject) => {
		model.findOneAndUpdate(query, updateData, (err, data) => {
			if (err) {
				reject(err)
			}
			resolve(data)
		})
	})
}

// 分页查找
const search = (model, query = {}, { pageSize = 10, pageNum = 1 }) => {
	return new Promise((resolve, reject) => {
		model
			.find(query)
			.skip((pageNum - 1) * pageSize)
			.limit(pageSize)
			.exec((err, data) => {
				if (err) {
					reject(err)
				}
				resolve(data)
			})
	})
}

// 得到符合条件的 total
const count = (model, query = {}) => {
	return new Promise((resolve, reject) => {
		model
			.where(query)
			.count((err, data) => {
				if (err) {
					reject(err)
				}
				resolve(data)
			})
	})
}

module.exports = {
	save,
	query,
	hasOne,
	deleteOne,
	deleteMany,
	updateOne,
	search,
	count,
}


