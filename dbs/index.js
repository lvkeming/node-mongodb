const mongoose = require('mongoose');

// 自购服务器11月过期
const dbConfig = {
	username: 'lianghaihang', // 用户名
	password: 'lhh123456', // 密码
	host: '175.24.174.41',
	port: 3717,
	name: 'airService', // 连接的数据库
	auth: 'airService', // 赋予角色权限的数据库
}

// mongodb compass 连接uri 
// mongodb://lianghaihang:lhh123456@175.24.174.41:3717/airService?authSource=airService&readPreference=primary&ssl=false

const uri = `mongodb://${dbConfig.username}:${dbConfig.password}@${dbConfig.host}:${dbConfig.port}/${dbConfig.name}?authSource=${dbConfig.auth}&readPreference=primary&ssl=false`

mongoose.connect(uri, (err) => {
	if (err) {
		console.log('connect fail', err);
		return
	}
	console.log('connect success');
});

module.exports = mongoose

// mongodb compass 下载地址  https://downloads.mongodb.com/compass/mongodb-compass-1.31.1-win32-x64.zip

// 数据库账号 db.auth('lianghaihang','lhh123456')
// 数据库权限 只在 airService 进行 readWrite


// mongodb 免费服务器
// const dbConfig = {
// 	username: 'lvkeming', // 用户名
// 	password: '24keming', // 密码
// }
// const dbName = 'myFirstDatabase'
// const uri = `mongodb+srv://${dbConfig.username}:${dbConfig.password}@cluster0.qkwmi.mongodb.net/${dbName}?retryWrites=true&w=majority`;
// mongodb compass 连接uri 
// mongodb+srv://lvkeming:24keming@cluster0.qkwmi.mongodb.net/myFirstDatabase?authSource=admin&replicaSet=atlas-l6bee2-shard-0&w=majority&readPreference=primary&retryWrites=true&ssl=true
