// 维修单
const mongoose = require('../index.js');

/* 维修单状态（进度）说明:
	1-生成维修单	申报
	2-后台确认     	维修中 
	3-完成后变成 	已维修
*/
const RepairSchema = new mongoose.Schema({
	id: {
		type: String,
		unique: true,
		required: true,
	},
	// 关联 维修员工 username
	repairStaff: {
		type: String,
	},
	// 关联故障
	faultList: {
		type: Array,
		required: true,
	},
	// 关联网点
	addressId: String,
	// 地址详情
	addressDesc: {
		type: String,
		required: true,
	},
	// 维修单状态（进度）
	status: {
		type: Number,
		default: 1
	},
})

module.exports = mongoose.model('Repair', RepairSchema, 'repair');