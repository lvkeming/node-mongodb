// 重要通知
const mongoose = require('../index.js');

const NoticeSchema = new mongoose.Schema({
	id: {
		type: String,
		unique: true,
		required: true,
	},
	title: {
		type: String,
		unique: true,
		required: true,
		validate: function (value) {
			return value.length >= 2
		}
	},
	content: {
		type: String,
		required: true
	},
	createTime: String,
})

module.exports = mongoose.model('Notice', NoticeSchema, 'notice');
