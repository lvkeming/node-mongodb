const mongoose = require('../index.js');

//数据校验
/**
 * 	unique 是否唯一
 * 	required 表示这个数据必须传入
 * 	max 最大值
 * 	min 最小值
 * 	enum 枚举类型，要求数据必须满足枚举值
 * 	match 增加的数据必须符合 match（正则）的规则
 * 	maxlength 最大长度
 * 	minlength 最小长度
 *  validate 自定义校验，返回bool类型
 */

// role 角色权限，与auth匹配 1-管理员 2-维修人员
const UserSchema = new mongoose.Schema({
	username: {
		type: String,
		unique: true,
		required: true,
		validate: function (value) {
			return value.length > 2
		}
	},
	password: {
		type: String,
		required: true,
		minlength: 6,
	},
	name: {
		type: String,
		required: true,
	},
	age: Number,
	sex: String, 
	tel: String,
	role: {
		type: String,
		default: '2'
	},
	repairCnt: {
		type: Number,
		default: 0,
		validate: function (value) {
			return value >= 0
		}
	},
	avatar: String,
	// 维修员工 所在网点 关联网点id
	addressId: {
		type: String,
		required: true,
	},
	status: {
		type: Number,
		default: 1
	},
});

// 第三个参数才是对应的集合，如果不设置第三个参数则对应的集合是 第一个参数+‘s’，即 users
module.exports = mongoose.model('User', UserSchema, 'user');