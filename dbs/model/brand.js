const mongoose = require('../index.js');

const BrandSchema = new mongoose.Schema({
	id: {
		type: Number,
		unique: true,
		required: true,
	},
	name: {
		type: String,
		required: true,
	},
	status: {
		type: Number,
		default: 1
	},
	img: String,
});

module.exports = mongoose.model('Brand', BrandSchema, 'brand');