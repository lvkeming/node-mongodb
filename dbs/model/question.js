// 提问
const mongoose = require('../index.js');

const QuestionSchema = new mongoose.Schema({
	id: {
		type: String,
		unique: true,
		required: true,
	},
	desc: String, // 问题描述
})

module.exports = mongoose.model('Question', QuestionSchema, 'question');