// 网点
const mongoose = require('../index.js');

const AddressSchema = new mongoose.Schema({
	id: {
		type: String,
		unique: true,
		required: true,
	},
	name: {
		type: String,
		unique: true,
		required: true,
		validate: function (value) {
			return value.length >= 2
		}
	},
	province: String, // 省份
	city: String, // 城市
	area: String, // 区域
	desc: String, // 详细地址
	tel: String,
})

module.exports = mongoose.model('Address', AddressSchema, 'address');
