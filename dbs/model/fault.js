// 故障
const mongoose = require('../index.js');

const FaultSchema = new mongoose.Schema({
	id: {
		type: String,
		unique: true,
		required: true,
	},
	// 维修价格
	price: {
		type: Number,
		required: true,
		validate: function (value) {
			return value >= 0
		}
	},
	desc: String, // 故障描述
})

module.exports = mongoose.model('Fault', FaultSchema, 'fault');