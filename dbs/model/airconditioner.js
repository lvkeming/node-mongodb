// airConditioner
const mongoose = require('../index.js');

const AirSchema = new mongoose.Schema({
	id: {
		type: String,
		unique: true,
		required: true,
	},
	name: String,
	// 关联品牌 id
	brand: {
		type: Number,
		required: true,
	},
	price: {
		type: Number,
		validate: function (value) {
			return value >= 0
		}
	},
	// 销量
	saleCnt: {
		type: Number,
		validate: function (value) {
			return value >= 0
		}
	},
	img: String,
})

module.exports = mongoose.model('Air', AirSchema, 'air');