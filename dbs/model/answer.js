// 回答
const mongoose = require('../index.js');

const AnswerSchema = new mongoose.Schema({
	id: {
		type: String,
		unique: true,
		required: true,
	},
	// 关联问题 id
	questionId: {
		type: String,
		required: true,
	},
	content: String, // 回答
})

module.exports = mongoose.model('Answer', AnswerSchema, 'answer');