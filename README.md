# nodeFormat

node 端模版
npm i 下载依赖
npm start 启动项目

# 一些关于code报错
code为11000，一般是我们定义 unique: true（唯一键）出现重复的情况了

# 获取前端给后端的数据
	1.ctx.request.query 获取get请求的数据，是一个对象
	2.ctx.request.body 获取post请求的数据，是一个对象
# 测试注意点

使用 postman 测试 post 接口时
需要选择 body 下面的 x-www-form-urlencoded，因为这样的接口好像只能拿到 form 表单提交的数据。
如果发送的参数存在数组，调试时最好配合前端进行调试。
因为 postman 使用 form 表单提交数组时，非常麻烦，要将数组里面每一个 item 单独拆分出来提交，例子如下。

```js
	role: user
	menuList[0][id]: 1
	menuList[0][title]: 首页
	menuList[0][url]: /home
	menuList[0][icon]: el-icon-s-home
	menuList[0][level]: 1
	menuList[1][id]: 3
	menuList[1][title]: 系统管理
	menuList[1][url]: /iconManage
	menuList[1][icon]: el-icon-s-tools
	menuList[1][level]: 1
	menuList[1][children][0][id]: 31
	menuList[1][children][0][title]: 图标管理
	menuList[1][children][0][url]: /iconManage
	menuList[1][children][0][icon]: el-icon-s-grid
	menuList[1][children][0][level]: 2
	menuList[1][children][1][id]: 32
	menuList[1][children][1][title]: 数据战报
	menuList[1][children][1][url]: /dataReport
	menuList[1][children][1][icon]: el-icon-s-data
	menuList[1][children][1][level]: 2
```

## 后端接口问题

前端发送 post 请求时，会先发送一个预请求，该预请求的类型是 OPTIONS 。只有 OPTIONS 通过的时候才会继续往下走 post 请求。
因此后端需要放开 OPTIONS 请求，也就是手动设置 OPTIONS请求的状态码为 200，同时还要放开其他的请求。
实现如下（完整代码 看 app.js）：
```js
	// 添加请求头 
	ctx.set('Access-Control-Allow-Headers', 'Content-Type, Content-Length, Authorization, Accept, X-Requested-With');
	ctx.set('Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS');

	if (ctx.request.method === 'OPTIONS') {
		// 解决options请求，状态码给200，然后才能往下走到post请求里面
		ctx.body = 200
	} else {
		// 放开其他请求
		await next()
	}
```