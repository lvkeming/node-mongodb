const router = require('koa-router')()
const multer = require('@koa/multer');
const fs = require('fs')
const path = require('path')
const { successFormat, errorFormat } = require('../common/dataFormat.js')
const serverImgPath = 'http://175.24.174.41/images/'

// 如果你未创建文件上传需要保存的文件夹或文件，使用dest时，会根据dest配置的路径自动创建
// 但是如果使用storage，必须确保文件夹或文件是否存在，否则会报错！
// 关于路径的坑：存放文件的路径是项目的根目录
// 使用 __dirname ，那就是当前所在的绝对路径
const storage = multer.diskStorage({
	// multer调用diskStorage可控制磁盘存储引擎
	destination: function (req, file, cb) {
		// cb(null, path.join('/Users/lvkeming/Desktop/vue_test/public/imgs'))
		cb(null, path.join(__dirname, '../', 'static/images'))
	},
	filename: function (req, file, cb) {
		cb(null, file.fieldname + '-' + Date.now() + '.jpg') // 文件名使用cb回调更改，参数二是文件名，为了保证命名不重复，使用时间戳
	}
})

// 指定一些数据大小（做限制的），object类型
const limits = {
	fields: 10,//非文件字段的数量
	fileSize: 500 * 1024,//文件大小 单位 b
	files: 1//文件数量
}

/*
上边file 输出：
{
	fieldname: 'file',
	originalname: 'xm.jpg',
	encoding: '7bit',
	mimetype: 'image/jpeg'
}
*/

const upload = multer({
	storage
});

// upload.single('file') 参数file是前端上传的文件字段名 element上传组件中的name		
// 注意，这个字段名前后端必须一致
/*
ctx.file 这里可以拿到我们存放图片的文件名，以及文件路径
	{
		fieldname: 'file',
		originalname: 'icon.jpg',
		encoding: '7bit',
		mimetype: 'image/jpeg',
		destination: './static/images/',
		filename: 'file-1649666885774.jpg',
		path: 'static/images/file-1649666885774.jpg',
		size: 510650
	}
*/
router.post('/upload', upload.single('file'), async (ctx, next) => {
	let { } = ctx.request.body
	const { filename, path } = ctx.file
	console.log(ctx.file);
	ctx.response.body = {
		...successFormat,
		data: {
			filename, path, imgSrc: serverImgPath + filename
		}
	}
})

router.post('/remove', async (ctx, next) => {
	let { filename, path } = ctx.request.body
	console.log(ctx.request.body);
	let flag = true, error
	await fs.unlink(path, (err) => {
		if (err) {
			flag = false
			error = err
		}
	})
	ctx.response.body = flag ? { ...successFormat } : { ...errorFormat, data: error }
})

module.exports = router

/**
 * ele + vue 调用例子
 *
 *  // 组件使用
	<el-upload
		class="upload-demo"
		action="http://localhost:2233/upload"
		:limit="1"
		:file-list="fileList"
		:on-success="uploadImgSuccess"
		:on-remove="removeImg"
		>
		<el-button size="small" type="primary">点击上传</el-button>
		<div slot="tip" class="el-upload__tip">只能上传jpg/png文件</div>
	</el-upload>

	// data定义
	data() {
		return {
			cnt: 0,
			fileList: [],
			imgName: "",
			path: ""
		};
	},

	// method
	uploadImgSuccess(res) {
		console.log("uploadImgSuccess", res);
		if (res.success) {
			this.imgName = res.data.filename;
			this.path = res.data.path;
		}
	},
	removeImg() {
		console.log("removeImg---");
		request
			.post("http://localhost:2233/remove", { path: this.path })
			.then(res => {
				console.log(res);
			})
			.catch(err => {
				console.log(err);
			});
	}
*/