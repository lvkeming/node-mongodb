const router = require('koa-router')()
const { successFormat, errorFormat } = require('../common/dataFormat.js')
const { query, save, updateOne, deleteOne, hasOne, search, count } = require('../dbs/sql.js')
const NoticeModel = require('../dbs/model/notice.js')
const { formateDate } = require('../common/tools')

// 按照搜索条件查询 
router.post('/notice/search', async (ctx, next) => {
	let { title = '', startTime = '', endTime = '', pageSize = 10, pageNum = 1 } = ctx.request.body

	try {
		let res = await query(NoticeModel, {})
		let list = res.filter(item => {
			if (startTime === '') {
				return (item.title || '').includes(title)
			} else {
				return (item.title || '').includes(title) && Number(item.id) <= (endTime + 24 * 60 * 60 * 1000) && Number(item.id) >= startTime
			}
		})

		ctx.response.body = {
			...successFormat,
			total: list.length,
			data: list.slice((pageNum - 1) * pageSize, pageNum * pageSize),
		}
	} catch (error) {
		ctx.response.body = { ...errorFormat, data: error }
	}
})

router.post('/notice/add', async (ctx, next) => {
	let { title, content } = ctx.request.body
	let id = Date.now()
	let article = new NoticeModel({ title, content, id, createTime: formateDate(id) })
	try {
		let res = await save(article)
		ctx.response.body = { ...successFormat, data: res }
	} catch (error) {
		if (error?.code === 11000) {
			ctx.response.body = { ...errorFormat, message: '通知标题已重复' }
		} else {
			ctx.response.body = { ...errorFormat, data: error }
		}
	}
})

router.post('/notice/del', async (ctx, next) => {
	let { id } = ctx.request.body
	if (!id) {
		ctx.response.body = { ...errorFormat, message: '通知编码不能为空！' }
	}
	try {
		let res = await deleteOne(NoticeModel, { id })
		ctx.response.body = { ...successFormat, data: res }
	} catch (error) {
		ctx.response.body = { ...errorFormat, data: error }
	}
})

router.post('/notice/update', async (ctx, next) => {
	let { title, content, id } = ctx.request.body
	if (!id) {
		ctx.response.body = { ...errorFormat, message: '通知编码不能为空！' }
	}
	let query = {
		id
	}
	let updateData = {
		content, title
	}
	try {
		let res = await updateOne(NoticeModel, query, updateData)
		ctx.response.body = { ...successFormat, data: res }
	} catch (error) {
		ctx.response.body = { ...errorFormat, data: error }
	}
})

router.get('/notice/detail', async (ctx, next) => {
	let { id } = ctx.request.query
	try {
		let res = await hasOne(NoticeModel, { id })
		if (res) {
			ctx.response.body = { ...successFormat, data: res }
		} else {
			ctx.response.body = { ...errorFormat, message: "该通知不存在" }
		}
	} catch (error) {
		ctx.response.body = { ...errorFormat, data: error }
	}
})


module.exports = router