const router = require('koa-router')()
const { successFormat, errorFormat } = require('../common/dataFormat.js')
const { query, save, updateOne, deleteOne, hasOne, search, count } = require('../dbs/sql.js')
const AddressModel = require('../dbs/model/address.js')

router.get('/address/list', async (ctx, next) => {
	let list = await query(AddressModel)
	ctx.response.body = { ...successFormat, data: list }
})

router.post('/address/search', async (ctx, next) => {
	let { province = '', city = '', name = '', id = '', pageSize = 10, pageNum = 1 } = ctx.request.body
	let params = { province, city, id }

	province === '' && delete params['province']
	city === '' && delete params['city']
	id === '' && delete params['id']

	try {
		let res = await query(AddressModel, params) || []
		let list = res.filter(item => {
			return (item.name || '').includes(name)
		})
		
		ctx.response.body = {
			...successFormat,
			total: list.length,
			data: list.slice((pageNum - 1) * pageSize, pageNum * pageSize),
		}
	} catch (error) {
		ctx.response.body = { ...errorFormat, data: error }
	}
})

router.post('/address/add', async (ctx, next) => {
	let { province = '', city = '', area = '', desc = '', id = '', name = '', tel = '' } = ctx.request.body
	let item = new AddressModel({ id, province, city, area, desc, name, tel })
	try {
		let res = await save(item)
		ctx.response.body = { ...successFormat, data: res }
	} catch (error) {
		if (error?.code === 11000) {
			ctx.response.body = { ...errorFormat, message: '网点编码已重复' }
		} else {
			ctx.response.body = { ...errorFormat, data: error }
		}
	}
})

router.post('/address/update', async (ctx, next) => {
	let { id, province = '', city = '', area = '', desc = '', name = '', tel = '' } = ctx.request.body
	let query = { id }
	let updateData = { province, city, area, desc, name, tel }
	try {
		let res = await updateOne(AddressModel, query, updateData)
		ctx.response.body = { ...successFormat, data: res }
	} catch (error) {
		ctx.response.body = { ...errorFormat, data: error }
	}
})

router.post('/address/del', async (ctx, next) => {
	let { id } = ctx.request.body
	try {
		let res = await deleteOne(AddressModel, { id })
		ctx.response.body = { ...successFormat, data: res }
	} catch (error) {
		ctx.response.body = { ...errorFormat, data: error }
	}
})

module.exports = router