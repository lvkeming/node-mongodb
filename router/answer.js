const router = require('koa-router')()
const { successFormat, errorFormat } = require('../common/dataFormat.js')
const { query, save, updateOne, deleteOne, hasOne, search, count } = require('../dbs/sql.js')
const AnswerModel = require('../dbs/model/answer.js')

router.post('/answer/search', async (ctx, next) => {
	let { questionId = '', pageSize = 10, pageNum = 1 } = ctx.request.body
	let query = { questionId }

	questionId === '' && delete query['questionId']

	try {
		let list = await search(AnswerModel, query, { pageSize, pageNum })
		let total = await count(AnswerModel, query)
		ctx.response.body = { ...successFormat, data: list, total }
	} catch (error) {
		ctx.response.body = { ...errorFormat, data: error }
	}
})

router.post('/answer/add', async (ctx, next) => {
	let { content, questionId } = ctx.request.body
	let item = new QuestionModel({ id: Date.now(), content, questionId })
	try {
		let res = await save(item)
		ctx.response.body = { ...successFormat, data: res }
	} catch (error) {
		ctx.response.body = { ...errorFormat, data: error }
	}
})

module.exports = router