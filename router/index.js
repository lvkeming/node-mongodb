const router = require('koa-router')()

router.get('/', async (ctx, next) => {
	ctx.response.body = `<h1>hello nodejs</h1>`
})

router.get('/404', async (ctx, next) => {
	ctx.response.body = `<h1>this url not find</h1>`
})

// ctx.request.body 可以接收 post 提交的数据
router.post('/test', async (ctx, next) => {
	console.log('submit', ctx.request.body);
	ctx.response.body = { name: 'mera' }
})


module.exports = router