const router = require('koa-router')()
const { successFormat, errorFormat } = require('../common/dataFormat.js')
const { query, save, updateOne, deleteOne, hasOne, search, count } = require('../dbs/sql.js')
const AirModel = require('../dbs/model/airconditioner.js')

router.post('/air/search', async (ctx, next) => {
	let { name = '', brand = '', id = '', pageSize = 10, pageNum = 1 } = ctx.request.body
	let query = { brand, name, id }

	id === '' && delete query['id']
	brand === '' && delete query['brand']
	name === '' && delete query['name']

	try {
		let list = await search(AirModel, query, { pageSize, pageNum })
		let total = await count(AirModel, query)
		ctx.response.body = { ...successFormat, data: list, total }
	} catch (error) {
		ctx.response.body = { ...errorFormat, data: error }
	}
})

router.post('/air/add', async (ctx, next) => {
	let { id, name, brand, price, saleCnt, img, } = ctx.request.body
	let item = new AirModel({ id, name, brand, price, saleCnt, img, })
	try {
		let res = await save(item)
		ctx.response.body = { ...successFormat, data: res }
	} catch (error) {
		if (error?.code === 11000) {
			ctx.response.body = { ...errorFormat, message: '品牌编码已重复' }
		} else {
			ctx.response.body = { ...errorFormat, data: error }
		}
	}
})

router.post('/air/del', async (ctx, next) => {
	let { id } = ctx.request.body
	if (!id) {
		ctx.response.body = { ...errorFormat, message: '空调编码不能为空！' }
	}
	try {
		let res = await deleteOne(AirModel, { id })
		ctx.response.body = { ...successFormat, data: res }
	} catch (error) {
		ctx.response.body = { ...errorFormat, data: error }
	}
})

router.post('/air/update', async (ctx, next) => {
	let { id, name, brand, price, saleCnt, img } = ctx.request.body
	let query = { id }
	let updateData = { name, brand, price, saleCnt, img }
	try {
		let res = await updateOne(AirModel, query, updateData)
		ctx.response.body = { ...successFormat, data: res }
	} catch (error) {
		ctx.response.body = { ...errorFormat, data: error }
	}
})

module.exports = router