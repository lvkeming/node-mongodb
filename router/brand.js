const router = require('koa-router')()
const { successFormat, errorFormat } = require('../common/dataFormat.js')
const { query, save, updateOne, deleteOne, hasOne, search, count } = require('../dbs/sql.js')
const BrandModel = require('../dbs/model/brand.js')

router.get('/brand/list', async (ctx, next) => {
	let brandList = await query(BrandModel)
	ctx.response.body = { ...successFormat, data: brandList }
})

router.post('/brand/search', async (ctx, next) => {
	let { name = '', id = '', pageSize = 10, pageNum = 1 } = ctx.request.body
	let query = { name, id }

	name === '' && delete query['name']
	id === '' && delete query['id']

	try {
		let list = await search(BrandModel, query, { pageSize, pageNum })
		let total = await count(BrandModel, query)
		ctx.response.body = { ...successFormat, data: list, total }
	} catch (error) {
		ctx.response.body = { ...errorFormat, data: error }
	}
})

router.post('/brand/add', async (ctx, next) => {
	let { name, id, img } = ctx.request.body
	let item = new BrandModel({ name, id, img })
	try {
		let res = await save(item)
		ctx.response.body = { ...successFormat, data: res }
	} catch (error) {
		if (error?.code === 11000) {
			ctx.response.body = { ...errorFormat, message: '品牌编码已重复' }
		} else {
			ctx.response.body = { ...errorFormat, data: error }
		}
	}
})

router.post('/brand/update', async (ctx, next) => {
	let { name, id, img } = ctx.request.body
	let query = { id }
	let updateData = { name, img }
	try {
		let res = await updateOne(BrandModel, query, updateData)
		ctx.response.body = { ...successFormat, data: res }
	} catch (error) {
		ctx.response.body = { ...errorFormat, data: error }
	}
})

module.exports = router