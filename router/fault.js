const router = require('koa-router')()
const { successFormat, errorFormat } = require('../common/dataFormat.js')
const { query, save, updateOne, deleteOne, hasOne, search, count } = require('../dbs/sql.js')
const FaultModel = require('../dbs/model/fault.js')

router.get('/fault/list', async (ctx, next) => {
	let list = await query(FaultModel)
	ctx.response.body = { ...successFormat, data: list }
})

router.post('/fault/search', async (ctx, next) => {
	let { id = '', price = '', pageSize = 10, pageNum = 1 } = ctx.request.body

	try {
		let list = await search(FaultModel, {}, { pageSize, pageNum })
		let total = await count(FaultModel, {})
		ctx.response.body = { ...successFormat, data: list, total }
	} catch (error) {
		ctx.response.body = { ...errorFormat, data: error }
	}
})

router.post('/fault/add', async (ctx, next) => {
	let { price, desc, id, img } = ctx.request.body
	let item = new FaultModel({ price, desc, id })
	try {
		let res = await save(item)
		ctx.response.body = { ...successFormat, data: res }
	} catch (error) {
		if (error?.code === 11000) {
			ctx.response.body = { ...errorFormat, message: '故障编码已重复' }
		} else {
			ctx.response.body = { ...errorFormat, data: error }
		}
	}
})

router.post('/fault/update', async (ctx, next) => {
	let { price = '', desc = '', id } = ctx.request.body
	let query = { id }
	let updateData = { price, desc }
	try {
		let res = await updateOne(FaultModel, query, updateData)
		ctx.response.body = { ...successFormat, data: res }
	} catch (error) {
		ctx.response.body = { ...errorFormat, data: error }
	}
})

router.post('/fault/del', async (ctx, next) => {
	let { id } = ctx.request.body
	try {
		let res = await deleteOne(FaultModel, { id })
		ctx.response.body = { ...successFormat, data: res }
	} catch (error) {
		ctx.response.body = { ...errorFormat, data: error }
	}
})

module.exports = router