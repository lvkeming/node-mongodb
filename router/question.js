const router = require('koa-router')()
const { query, save, updateOne, deleteOne, hasOne, search, count } = require('../dbs/sql.js')
const { successFormat, errorFormat } = require('../common/dataFormat.js')
const QuestionModel = require('../dbs/model/question.js')

router.post('/question/search', async (ctx, next) => {
	let { id = '', desc = '', pageSize = 10, pageNum = 1 } = ctx.request.body
	let query = { id, desc }

	desc === '' && delete query['desc']
	id === '' && delete query['id']

	try {
		let list = await search(QuestionModel, query, { pageSize, pageNum })
		let total = await count(QuestionModel, query)
		ctx.response.body = { ...successFormat, data: list, total }
	} catch (error) {
		ctx.response.body = { ...errorFormat, data: error }
	}
})

router.post('/question/add', async (ctx, next) => {
	let { desc } = ctx.request.body
	let item = new QuestionModel({ id: Date.now(), desc })
	try {
		let res = await save(item)
		ctx.response.body = { ...successFormat, data: res }
	} catch (error) {
		if (error?.code === 11000) {
			ctx.response.body = { ...errorFormat, message: '问题编码已重复' }
		} else {
			ctx.response.body = { ...errorFormat, data: error }
		}
	}
})

module.exports = router