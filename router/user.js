const router = require('koa-router')()
const { successFormat: formatResult, errorFormat } = require('../common/dataFormat.js')
const { query, save, updateOne, deleteOne, hasOne, count } = require('../dbs/sql.js')
const UserModel = require('../dbs/model/user.js')
const { findOne } = require('../dbs/model/user.js')

// 得到全部用户
router.get('/user/list', async (ctx, next) => {
	let userList = await query(UserModel)
	// 直接把数据暴露出去，外部就能拿到。
	ctx.response.body = { ...formatResult, data: userList }
})

// 根据维修网点 得到全部维修人员
router.get('/staff/list', async (ctx, next) => {
	let { addressId = '' } = ctx.request.body
	let params = { addressId, role: '2' }

	addressId === '' && delete params['addressId']

	let userList = await query(UserModel, params)
	ctx.response.body = { ...formatResult, data: userList }
})

// 维修员工查询
router.post('/staff/search', async (ctx, next) => {
	let { name = '', sex = '', addressId = '', pageSize = 10, pageNum = 1 } = ctx.request.body
	let params = { sex, addressId, role: '2' }

	sex === '' && delete params['sex']
	addressId === '' && delete params['addressId']

	try {
		let res = await query(UserModel, params)
		let list = res.filter(item => {
			return (item.name || '').includes(name)
		})

		ctx.response.body = {
			...formatResult,
			total: list.length,
			data: list.slice((pageNum - 1) * pageSize, pageNum * pageSize),
		}
	} catch (error) {
		ctx.response.body = { ...errorFormat, data: error }
	}
})

// 增加员工
router.post('/staff/add', async (ctx, next) => {
	let { username, password, name, age, sex, tel, addressId, avatar } = ctx.request.body
	let user = new UserModel({
		username,
		password,
		name,
		age,
		sex,
		tel,
		addressId,
		avatar,
	})
	try {
		let res = await save(user)
		ctx.response.body = { ...formatResult, data: res }
	} catch (error) {
		if (error?.code === 11000) {
			ctx.response.body = { ...errorFormat, message: '用户名已重复' }
		} else {
			ctx.response.body = { ...errorFormat, data: error }
		}
	}
})

// 更新信息
router.post('/user/update', async (ctx, next) => {
	let { username, password, name, tel, repairCnt, addressId, avatar, } = ctx.request.body

	let query = { username }
	let updateData = {
		password, name, tel, repairCnt, addressId, avatar,
	}

	try {
		let res = await updateOne(UserModel, query, updateData)
		ctx.response.body = { ...formatResult, data: res }
	} catch (error) {
		ctx.response.body = { ...errorFormat, data: error }
	}
})

// 修改密码
router.post('/changePassword', async (ctx, next) => {
	let { username, password, newPassword } = ctx.request.body
	try {
		let user = await hasOne(UserModel, { username })
		if (user.password !== password) {
			ctx.response.body = { ...errorFormat, message: '旧密码不正确' }
		} else {
			let res = await updateOne(UserModel, { username }, { password: newPassword })
			ctx.response.body = { ...formatResult, data: res }
		}
	} catch (error) {
		ctx.response.body = { ...errorFormat, data: error }
	}
})

// 删除用户
router.post('/user/del', async (ctx, next) => {
	let { username } = ctx.request.body
	if (!username) {
		ctx.response.body = { ...errorFormat, message: '请使用form表单提交正确数据' }
		return
	}
	// 如果想要拿到报错数据，用 try catch，不然拿不到错误，从而不能给客户端提示
	// 因为 await 只能拿到 promise 的 resolve 数据，拿不到reject数据
	// 用 try 去执行数据库操作代码，然后用 catch 拿到错误信息（前提是有错误信息）
	try {
		let res = await deleteOne(UserModel, { username })
		ctx.response.body = { ...formatResult, data: res }
	} catch (error) {
		ctx.response.body = { ...errorFormat, data: error }
	}
})

// 登录
router.post('/user/login', async (ctx, next) => {
	let { username, password } = ctx.request.body
	try {
		let res = await query(UserModel, { username })

		if (res.length > 0) {
			if (res[0].password === password) {
				ctx.response.body = { ...formatResult, data: res[0] }
			} else {
				ctx.response.body = { ...errorFormat, message: '用户名或密码错误' }
			}
		} else {
			ctx.response.body = { ...errorFormat, message: '用户名不存在' }
		}
	} catch (error) {
		console.log(error);
		ctx.response.body = { ...errorFormat }
	}
})

// 得到某一用户
router.get('/user/find', async (ctx, next) => {
	let { username } = ctx.request.query
	try {
		let res = await hasOne(UserModel, { username })
		if (res) {
			ctx.response.body = { ...formatResult, data: res }
		} else {
			ctx.response.body = { ...errorFormat, message: '该用户不存在', data: res }
		}

	} catch (error) {
		ctx.response.body = { ...errorFormat, message: 'system error' }
	}
})

module.exports = router