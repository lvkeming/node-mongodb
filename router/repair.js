const router = require('koa-router')()
const { successFormat, errorFormat } = require('../common/dataFormat.js')
const { query, save, updateOne, deleteOne, hasOne, search, count } = require('../dbs/sql.js')
const RepairModel = require('../dbs/model/repair.js')
const UserModel = require('../dbs/model/user.js')

router.post('/repair/search', async (ctx, next) => {
	let {
		id = '', repairStaff = '', faultList = [], status = '', pageSize = 10, pageNum = 1
	} = ctx.request.body

	let params = { id, status, repairStaff }

	id === '' && delete params['id']
	status === '' && delete params['status']
	repairStaff === '' && delete params['repairStaff']

	try {
		let res = await query(RepairModel, params)
		let list = res.filter(item => {
			if (faultList.length === 0) return true
			else {
				// 数组的交集
				let temp = (item.faultList || []).filter(fault => faultList.includes(fault))
				return temp.length > 0
			}
		})
		ctx.response.body = {
			...successFormat,
			total: list.length,
			data: list.slice((pageNum - 1) * pageSize, pageNum * pageSize),
		}
	} catch (error) {
		ctx.response.body = { ...errorFormat, data: error }
	}
})

router.post('/repair/add', async (ctx, next) => {
	let { faultList = [], addressDesc = '', repairStaff = '', addressId = '' } = ctx.request.body

	let params = { faultList, addressDesc, repairStaff, addressId }

	repairStaff === '' && delete params['repairStaff']
	addressId === '' && delete params['addressId']
	addressDesc === '' && delete params['addressDesc']

	let item = new RepairModel({ id: Date.now(), ...params })
	try {
		let res = await save(item)
		ctx.response.body = { ...successFormat, data: res }
	} catch (error) {
		if (error?.code === 11000) {
			ctx.response.body = { ...errorFormat, message: '维修单编码已重复' }
		} else {
			ctx.response.body = { ...errorFormat, data: error }
		}
	}
})

router.post('/repair/update', async (ctx, next) => {
	let { id, repairStaff = '', status = '', addressId = '', addressDesc = '', faultList = [], } = ctx.request.body
	let query = { id }
	let updateData = { repairStaff, status, faultList, addressId, addressDesc }

	repairStaff === '' && delete updateData['repairStaff']
	status === '' && delete updateData['status']
	addressId === '' && delete updateData['addressId']
	addressDesc === '' && delete updateData['addressDesc']
	faultList.length === 0 && delete updateData['faultList']

	try {
		let res = await updateOne(RepairModel, query, updateData)
		if (status === 3) {
			// 维修完成 更新维修人员的维修数量
			let staff = await hasOne(UserModel, { username: repairStaff })
			let { repairCnt } = staff
			await updateOne(UserModel, { username: repairStaff }, { repairCnt: repairCnt + 1 })
		}
		ctx.response.body = { ...successFormat, data: res }
	} catch (error) {
		ctx.response.body = { ...errorFormat, data: error }
	}
})

module.exports = router